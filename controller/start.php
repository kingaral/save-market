<?php
	
    define("HOME", $_SERVER['DOCUMENT_ROOT']);
	require_once HOME."/controller/config.php";
	require_once HOME."/model/core_functions.php";
	require_once HOME."/model/my_functions.php";
	require_once HOME."/controller/request.php";
    if (!$user && $_SERVER['SCRIPT_NAME'] != '/index.php') redirect('/index.php');
?>