<?php
	$request = xss($_REQUEST);
	// $request = $_REQUEST;
	$id = (isset($request['id'])) ? intval($request['id']) : 0;
    $message = (isset($request["message"])) ? '<div class="alert alert-info">'.$request["message"].'</div>' : '';
	$myPage = (isset($request["page"])) ? $request["page"] : false;

    if (isset($_SESSION['userid']) && is_numeric($_SESSION['userid'])) $user = getUser($_SESSION['userid']);
    else $user = false;

	if (isset($request['auth'])) {
	    login($request['login'], $request['password']);
        redirect('/');
    }

    if (isset($request['logout'])) {
	    logout();
        redirect('/');
    }

    if (isset($request['btn-contact'])) {
        $data['title'] = $request['title'];
        $data['msg'] = $request['msg'];
        $data['email'] = $request['email'];
        if (addContact($data)) redirect('/?page=' . $myPage . '&message=Your message sent successfully!');
        redirect('/?page=' . $myPage . '&message=Error while sending message!');
    }

    if (isset($request['send-msg-item'])) {
	    $data['msg'] = $request['msg'];
	    $data['user'] = $user['id'];
	    $data['author'] = $request['user'];
	    if (addDialog($data)) redirect('?page='.$myPage.'&id='.$id.'&message=Message sent successfully');
	    redirect('/?page='.$myPage.'&id='.$id.'&message=Error while sending message');
    }

    if (isset($request['add-item-link'])) {
	    redirect('/?page=add-item');
    }

    if (isset($request['add-item-btn'])) {
	    $data['title'] = $request['title'];
	    $data['cat'] = $request['cat'];
	    $data['info'] = $request['info'];
	    $data['time'] = time();
	    $data['user'] = $user['id'];
	    $data['city'] = $request['city'];
	    $data['reward'] = $request['reward'];
	    $data['coors'] = $request['coors'];
	    $data['type'] = 0;

	    $filename = substr(rand(11111, 99999).time(), 0, 10);
	    require_once "model/class.upload/class.upload.php";
        $handle = new upload($_FILES['photo']);
        if ($handle->uploaded) {
            $handle->file_new_name_body = $filename;
            $handle->process('files/');
            if ($handle->processed) {
                $data['photo'] = '/files/'.$filename.'.'.$handle->file_dst_name_ext;
                $handle->clean();
            } else {
                echo 'error : ' . $handle->error;
            }
        }



        if (addItem($data)) redirect('/?page=add-item&message=Added successfully!');
        redirect('/?page=add-item&message=Error while adding item!');
    }

    if (isset($request['searсh'])) {
	     $items = search($request);
    }

    if (isset($request['add-user'])) {
	    $data['fio'] = $request['fio'];
	    $data['login'] = $request['login'];
	    $data['password1'] = $request['password1'];
	    $data['password2'] = $request['password2'];
	    if (reg($data)) redirect('/?message=You successfully registered!');
        else redirect('/?page=reg&message=Error while registration');
    }
//-----------------------------------------------------------------------------------------//


    if ($myPage == 'detail') {
        $item = getItem($id);
    }

    if ($myPage == 'cat') {
        $cat = getCat($id);
    }

    if ($myPage == 'detail') {

    }

?>