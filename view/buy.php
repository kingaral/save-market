<?php
    $item =  getItem($id);

    // Mock payment gateway function
    function processPayment($cardNumber, $expMonth, $expYear, $cvc, $amount) {
        // Check if card details are valid
        if ($cardNumber === '4242424242424242' && $expMonth === '12' && $expYear === '2025' && $cvc === '123') {
            // Payment succeeded
            return true;
        } else {
            // Payment failed
            return false;
        }
    }

    if (isset($_POST['submit-pay'])) {   
        // Retrieve form input values
        $cardNumber = $_POST['card-number'];
        $expMonth = $_POST['exp-month'];
        $expYear = $_POST['exp-year'];
        $cvc = $_POST['cvc'];
        $amount = $item['reward']; // Replace with actual amount

        // Process payment using mock payment gateway
        $paymentResult = processPayment($cardNumber, $expMonth, $expYear, $cvc, $amount);

        // Check if payment succeeded and show message
        if ($paymentResult) {
            echo 'Payment succeeded!';
        } else {
            echo 'Payment failed. Please check your card details and try again.';
        }
    }
?>

<legend>Buy - <?=$item['title']?> (<?=$item['reward']?> tenge)</legend>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-4">
            <form action="" method="POST" class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h3 class="text-center">Payment Details</h3>
                        <div class="inlineimage"> <img class="img-responsive images" src="https://cdn0.iconfinder.com/data/icons/credit-card-debit-card-payment-PNG/128/Mastercard-Curved.png"> <img class="img-responsive images" src="https://cdn0.iconfinder.com/data/icons/credit-card-debit-card-payment-PNG/128/Discover-Curved.png"> <img class="img-responsive images" src="https://cdn0.iconfinder.com/data/icons/credit-card-debit-card-payment-PNG/128/Paypal-Curved.png"> <img class="img-responsive images" src="https://cdn0.iconfinder.com/data/icons/credit-card-debit-card-payment-PNG/128/American-Express-Curved.png"> </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group"> <label>Card Number</label>
                                    <div class="input-group"> <input type="tel" name="card-number" class="form-control" placeholder="Valid Card Number" /> <span class="input-group-addon"><span class="fa fa-credit-card"></span></span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group"> <label><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Date</label> <input type="tel" name="exp-month" class="form-control" placeholder="MM" /> </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group"> <label>&nbsp;</label> <input type="tel" name="exp-year" class="form-control" placeholder="YY" /> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group"> <label>CVC CODE</label> <input type="tel" name="cvc" class="form-control" placeholder="CVC" /> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group"> <label>CARD OWNER</label> <input type="text" name="card-owner" class="form-control" placeholder="Card Owner Name" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12"> <button class="btn btn-success btn-lg btn-block" type="submit" name="submit-pay">Confirm Payment</button> </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    .inlineimage{
        max-width:470px;
        margin-right: 8px;
        margin-left: 10px
    }
    .images{
        display: inline-block;
        max-width: 98%;
        height: auto;
        width: 22%;
        margin: 1%;
        left:20px;
        text-align: center
    }

</style>