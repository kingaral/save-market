<script type="text/javascript">
    var myMap;

    ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('map', {
            center: [<?=$item['coors']?>],
            zoom: 15
        });

        myPlacemark = new ymaps.Placemark([<?=$item['coors']?>]);
        myMap.geoObjects.add(myPlacemark);

    }
</script>

    <div class="row">
        <div class="col-md-8">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="detail-photo"><?=photoItem($item['id'])?></div>
                    <h4><?=$item['title']?></h4>
                    <p style="text-align: justify;"><?=$item['info']?></p>
                    <h4>Location:</h4>
                    <p><div id="map" style="height: 200px;"></div></p>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <?php if ($item['user'] != $user['id'] && $user) { ?>
            <form action="" method="post">
                <input type="hidden" name="user" value="<?=$item['user']?>">
                <div class="form-group">
                    <textarea name="msg" required placeholder="Write question to auhor" rows="3" class="form-control"></textarea>
                </div>
                <button name="send-msg-item" type="submit" class="btn btn-block btn-success">Write to author</button>
            </form>
                <br>
            <?php } ?>


            <div class="panel panel-default">
                <div class="panel-body" style="text-align: center;">
                    <img src="/assets/images/user.png" alt="">
                    <div style="margin-top: 10px;"><?=getUser($item['user'])['fio']?></div>
                </div>
            </div>

            <table class="table table-bordered">
                <tr>
                    <th>Posted time:</th>
                    <td><?=date(FORMAT_DATE, $item['time'])?></td>
                </tr>
                <tr>
                    <th>City:</th>
                    <td><?=$item['city']?></td>
                </tr>
                <tr>
                    <th>Cost:</th>
                    <td><?=$item['reward']?> tenge</td>
                </tr>
            </table>

            <?php if ($item['user'] != $user['id'] && $user) { ?>
                <a href="/?page=buy&id=<?=$item['id']?>" class="btn btn-block btn-info">Buy <?=$item['reward']?> tenge</a>
            <?php } ?>
        </div>
    </div>
<?php if (nodata($item)) nodata($item); ?>