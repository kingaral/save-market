<table class="index-menu">
    <?php 
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $i = 1; foreach (getCats() as $cat) { ?>
        <?=($i == 1) ? '<tr>' : '' ?>
        <td style="width:33%;">
            <a href="?page=cat&id=<?=$cat['id']?>" class="item-menu">
                <div class="icon"><img src="<?=$cat['logo']?>" alt=""></div>
                <div class="title"><?=$cat['title']?></div>
            </a>
        </td>
        <?php if ($i == 3) { echo '</tr>'; $i = 0; } ?>
    <?php $i++; } ?>
</table>


<legend style="margin-top: 40px">Latest announcements</legend>
<div class="table-responsive">
    <table class="last-ads">
        <?php $i = 1; foreach (getItems() as $item) { ?>
            <?=($i == 1) ? '<tr>' : '' ?>
            <td style="width:25%;">
                <a href="?page=detail&id=<?=$item['id']?>" class="ads-link">
                    <div class="ads-img"><?=photoItem($item['id'])?></div>
                    <div class="ads-title"><?=$item['title']?></div>
                </a>
            </td>
            <?php if ($i == 4) { echo '</tr>'; $i = 0; } ?>
        <?php $i++; } ?>
        <?=nodata(getItems())?>
    </table>
</div>