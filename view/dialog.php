<div class="row">
    <?php require_once "view/leftmenu.php"; ?>
    <div class="col-md-8">

        <legend><?=(isset($request['type'])) ? 'Outbox' : 'Inbox'?></legend>

        <?php foreach (getDialog($user['id']) as $dialog) { ?>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" style="width:50px;" src="/assets/images/user.png" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?=getUser($dialog['user'])['fio']?></h4>
                    <?=$dialog['msg']?>
                </div>
            </div>
        <?php } ?>
        <?=nodata(getDialog($user['id']))?>
    </div>

</div>
