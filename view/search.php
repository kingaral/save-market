<legend>Search</legend>

<div class="row">
    <form class="col-md-4" method="get">
        <div class="panel panel-default">
            <div class="panel-heading">Categories</div>
            <div class="panel-body">
                <?php foreach (getCats() as $cat) { ?>
                <div class="checkbox">
                    <label><input type="radio" name="cat" value="<?=$cat['id']?>"> <?=$cat['title']?></label>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Item type</div>
            <div class="panel-body">
                <div class="checkbox">
                    <label><input type="radio" name="type" value="0"> Will buy</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="type" value="1"> Selling</label>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">City</div>
            <div class="panel-body">
                <div class="checkbox">
                    <label><input type="radio" name="city" value="Almaty"> Almaty</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="city" value="Taldykorgan">Taldykorgan</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="city" value="Astana"> Astana</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="city" value="Semey"> Semey</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="city" value="Aktobe"> Aktobe</label>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Cost</div>
            <div class="panel-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="ot" placeholder="From">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="do" placeholder="To">
                </div>
            </div>
        </div>
        <input type="hidden" name="page" value="search">
        <input type="submit" name="searсh" class="btn btn-default btn-block" value="Search">
    </form>
    <div class="col-md-8">
        
        <?php if (!isset($request['searсh'])) { ?>
            <div style="text-align: center;">
                <img src="/assets/images/search.png" alt="">
                <h3 style="color: gray; font-style: italic;">Filters</h3>
            </div>
        <?php }else{ ?>

            <?php foreach ($items as $item) { ?>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <?=photoItem($item['id'], 'media-object')?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="?page=detail&id=<?= $item['id'] ?>"><?=$item['title']?></a></h4>
                        <div style="text-align: justify;"><?=mb_substr($item['info'], 0, 90)?>...</div>
                    </div>
                </div>
                <hr>
            <?php } ?>



        <?php } ?>

    </div>
</div>