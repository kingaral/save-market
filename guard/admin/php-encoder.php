<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>PHP encoder</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>PHP encoder&nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
                            
                            <form method="post" class="form-horizontal form-bordered">
                            
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">PHP encoder</h2>
									<p class="panel-subtitle">Protect against PHP coder, theft and modification.</p>
								</header>
								<div class="panel-body">
                                <fieldset>
<?php
@$_SESSION['phpcode-session'] = $_POST['phpcode'];

function encode_normal($code)
{
    $input   = stripslashes($_POST['phpcode']);
    $output  = $input;
    $output  = gzdeflate("" . $output . "", 9);
    $output  = base64_encode($output);
    $output  = "<?php eval(gzinflate(base64_decode('$output'))); ?>";
    $ilength = strlen($input);
    $olength = strlen($output);
    return $output;
}

function encode_advanced($code, $repeats)
{
    $input  = stripslashes($_POST['phpcode']);
    $output = $input;
    for ($counter = 0; $counter < $repeats - 1; $counter++) {
        $output = gzdeflate("" . $output . "", 9);
        $output = base64_encode($output);
        $output = "<?php eval(gzinflate(base64_decode('$output'))); ?>";
    }
    $output  = gzdeflate("" . $output . "", 9);
    $output  = base64_encode($output);
    $output  = "<?php eval(gzinflate(base64_decode('\n$output'))); ?>";
    $ilength = strlen($input);
    $olength = strlen($output);
    return $output;
}

?>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="phpcode" rows="10" type="text" required><?php
echo $_SESSION['phpcode-session'];
?></textarea>
                                        <br />
                                        <div class="col-md-12">
                                            
                                        <div class="col-md-6">
                                        <select name="type" required>
                                        <option value="normal" selected>Normal Strength - Encoded</option>
                                        <option value="advanced">high Strength - Encoded</option>
                                        </select>
                                        </div>
                                            
                                        <div class="col-md-6">
                                        <div class="form-group">
											<label class="col-sm-4 control-label">repetitions</label>
											<div class="col-sm-8">
												<div class="row">
                                                    <div class="m-md slider-primary" data-plugin-slider data-plugin-options='{ "value": 10, "range": "min", "max": 50 }' data-plugin-slider-output="#listenSlider">
														<input name="repeats" id="listenSlider" type="hidden" value="10" />
													</div>
													<p class="output"><b>10</b>repeat<br /></p>
                                                </div>
											</div>
										</div>
                                        </div>
                                        
                                        </div>
                                        
<?php
if (isset($_POST['encrypt'])) {
    
    $phpcode = stripslashes($_POST['phpcode']);
    $type    = $_POST['type'];
    $repeats = $_POST['repeats'];
    
    if ($type == "normal") {
        echo '<br /><br />
<b>Encoded PHP Code:</b>
<textarea class="form-control" name="phpcode-encoded" rows="10" type="text" readonly>' . encode_normal($phpcode) . '</textarea>
</script>
';
    } else {
        echo '<br /><br />
<b>Encoded PHP Code:</b>
<textarea class="form-control" name="phpcode-encoded" rows="10" type="text" readonly>' . encode_advanced($phpcode, $repeats) . '</textarea>
</script>
';
    }
    
}
?>     
                                    </div>
                                    <div class="col-md-4">
                                    <p>
                                        <ol>
                                        <li>Enter the PHP code you want to change.
                                        <li> <b>Encode</b> <b></b>press the button</li>
                                        </ol>
                                    </p>
                                    </div>
                                    
                                </fieldset>
								</div>
							</section>
                                
                            <input class="btn btn-primary" type="submit" name="encrypt" value="Encode">
                                
                            </form>

						</div>
						
						 
					
						
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>