<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Password generator</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>Password generator &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Fast Password generator</h2>
									<p class="panel-subtitle">Quickly generates lowercase numbers and passwords.</p>
								</header>
								<div class="panel-body">
                            <div class="row">
                            <div class="col-md-2">
                                <form method="post">
                                    <button type="submit" name="quick-generate" class="btn btn-primary">Generate<br />(8 symbols)</button>
                                    <input name="length" type="hidden" value="8">
                                </form>
                            </div>
							<div class="col-md-2">
                                <form method="post">
                                    <button type="submit" name="quick-generate" class="btn btn-primary">Generate<br />(12 symbols)</button>
                                    <input name="length" type="hidden" value="12">
                                </form>
                            </div>
                            <div class="col-md-2">
                                <form method="post">
                                    <button type="submit" name="quick-generate" class="btn btn-primary">Generate<br />(16 symbols)</button>
                                    <input name="length" type="hidden" value="16">
                                </form>
                            </div>
<?php
if (isset($_POST['quick-generate'])) {
    $chars     = "abcdefghijklmnopqrstuvwxyz0123456789";
    $length    = $_POST['length'];
    $gpassword = substr(str_shuffle($chars), 0, $length);
}
?>
							<div class="col-md-6">Password Generator: <input type="text" size="16" maxlength="16" class="form-control" placeholder="No generated password" value="<?php
echo @$gpassword;
?>" readonly></div>
                            </div>
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Upper case keyword generator</h2>
									<p class="panel-subtitle">Creating a strong password makes it difficult to crack or guess.</p>
								</header>
								<div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-8">
<?php
$passwords = array();
if (isset($_POST['advanced-generate'])) {
    $length         = $_POST['length'];
    $lcase_letters  = "abcdefghijklmnopqrstuvwxyz";
    $upcase_letters = strtoupper($lcase_letters);
    $digits         = "0123456789";
    $spec_chars     = $_POST['spec-chars'];
    $amount         = $_POST['amount'];
    $chars          = "";
    
    if (isset($_POST['lcase-letters']) && $_POST['lcase-letters'] == 'On')
        $chars .= $lcase_letters;
    
    if (isset($_POST['upcase-letters']) && $_POST['upcase-letters'] == 'On')
        $chars .= $upcase_letters;
    
    if (isset($_POST['digits']) && $_POST['digits'] == 'On')
        $chars .= $digits;
    
    if (isset($_POST['spec-chars']))
        $chars .= $spec_chars;
    
    for ($x = 1; $x <= $amount; $x++) {
        $len = strlen($chars);
        $pw  = "";
        for ($y = 1; $y <= $length; $y++) {
            $pw .= substr($chars, rand(0, $len - 1), 1);
            $pw = str_shuffle($pw);
        }
        $passwords[] = $pw;
    }
}
?>
                                        <form method="post" class="form-horizontal form-bordered">
								
										<div class="form-group">
											<label class="col-sm-4 control-label">	Count</label>
											<div class="col-sm-8">
												<div class="row">
                                                    <div class="m-md slider-primary" data-plugin-slider data-plugin-options='{ "value": 50, "range": "min", "max": 100 }' data-plugin-slider-output="#listenSlider">
														<input name="amount" id="listenSlider" type="hidden" value="50" />
													</div>
													<p class="output"><b>50</b> create a password</p>
                                                </div>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-4 control-label">length</label>
											<div class="col-sm-8">
												<div class="row">
                                                    <div class="m-md slider-primary" data-plugin-slider data-plugin-options='{ "value": 16, "range": "min", "max": 32 }' data-plugin-slider-output="#listenSlider2">
														<input name="length" id="listenSlider2" type="hidden" value="16" />
													</div>
													<p class="output2">every  <b>16</b>symbols</p>
                                                </div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">characters to include</label>
											<div class="col-sm-8">
												<div class="switch switch-sm switch-success">
														<input type="checkbox" name="lcase-letters" data-plugin-ios-switch checked="checked" value="On"/>
												</div> Lower Case  &nbsp;&nbsp;&nbsp;<kbd>abcdefghijklmnopqrstuvwxyz</kbd><br />
                                                <div class="switch switch-sm switch-success">
														<input type="checkbox" name="upcase-letters" data-plugin-ios-switch checked="checked" value="On"/>
												</div> Upper Case &nbsp;&nbsp;&nbsp;<kbd>ABCDEFGHIJKLMNOPQRSTUVWXYZ</kbd><br />
                                                <div class="switch switch-sm switch-success">
														<input type="checkbox" name="digits" data-plugin-ios-switch checked="checked" value="On"/>
												</div> Цифрлар &nbsp;&nbsp;&nbsp;<kbd>0123456789</kbd><br /><br />
                                                <input class="form-control" name="spec-chars" value="!#$%&()*+-=?[]{}|~">
                                                Add special characters (e.g. <kbd>!#$%&()*+-=?[]{}|~</kbd>). .characters must be repeated to increase the frequency of the password
											</div>
                                            
										</div>
                                        </div>
                                        <div class="col-md-4">
                                           Password generator(s):
                                            <div class="well">
                                            <?php
if (isset($_POST['advanced-generate'])) {
    $i = 1;
    foreach ($passwords as $pass) {
        echo "<b>" . $i . ". </b>" . $pass . "<br />";
        $i++;
    }
}
?>
                                            </div>
                                        </div>
                                    </div>
                                    
								</div>
                                <footer class="panel-footer">
										<button type="submit" name="advanced-generate" class="btn btn-primary">Generate</button>
                                        <button type="reset" class="btn btn-default">Clean</button>
								</footer>
                                </form>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">keyword detection rate</h2>
									<p class="panel-subtitle">It is recommended to use a safe and secure password</p>
								</header>
								<div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-5">
                                        <b>A strong password:</b>
                                            <ul>
                                                <li> <strong>15 symbols</strong></li>
                                                <li><strong>Upper case</strong></li>
                                                <li> <strong>lower case</strong></li>
                                                <li> <strong>numbers</strong></li>
                                                <li><strong>The previous password is not reliable</strong></li>
                                                <li><strong>Don't use names </strong></li>
                                                <li><strong>no entry allowed</strong></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-7">
                                           
                                        </div>
                                    </div>
                                    <br />
                                    <div class="well">
        <strong>To prevent password cracking from mass engineering, password cracking, you should identify the following:</strong>
        <ol>
		<li>Do not use the same password for multiple important accounts.</li>
		<li>Use a combination of 15 characters, including one uppercase letter, one lowercase letter, and one special character.</li>
		<li>Avoid using your name, birthdate, home address, phone number, or personal identification number (PIN) as your password.</li>
        <li>Do not use words from the dictionary in your passwords.</li>
		<li>Do not store passwords in your web browser (Firefox, Chrome, Safari, Opera, IE), as passwords saved in web browsers can be easily accessed.</li>
		<li>Do not log in to your personal accounts on other computers, public Wi-Fi, or with free VPN and proxy servers.</li>
		<li>Do not send sensitive information in online mode using HTTP or FTP, as this can easily be intercepted. Use HTTPS and SFTP for encrypted connections.</li>
		<li>Change your passwords at least every 10 weeks.</li>
		<li>If possible, enable two-factor authentication (2FA).</li>
		<li>Do not store critical passwords in email drafts or notes.</li>
		
        </ol>
            </div>
                                    
								</div>
							</section>

						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Tips and information</h2>
								</header>
								<div class="panel-body">
								To prevent password cracking use <strong>Password Generator</strong> 
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">How to save your password</h2>
								</header>
								<div class="panel-body">
                                    <ul>
                                        <li>use a password manager</li>
                                        <li>...or enter your own password.</li>
                                    </ul>
								</div>
							</section>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>