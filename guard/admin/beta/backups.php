<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Сақтық көшірмелер</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>Сақтық көшірмелер &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Сақтық көшірмелер</h2>
									<p class="panel-subtitle">Сақтық көшірмелерДерекқордағы Сақтық көшірмелер тізімі.</p>
								</header>
								<div class="panel-body">

                                    
								</div>
							</section>

						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Кеңестер мен ақпараттар</h2>
								</header>
								<div class="panel-body">
                                      <b>Сақтық көшірмелер</b> 	сақтық көшірме жасау процесі болып табылады және ол деректер жоғалғаннан кейін түпнұсқаны қалпына келтіру үшін пайдаланылуы мүмкін, сондықтан компьютерлік деректер көшіру және мұрағаттау жатады.
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Сақтық көшірме жасау</h2>
								</header>
								<div class="panel-body">
                                    
								</div>
							</section>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>