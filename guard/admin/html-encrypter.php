<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>HTML Encrypter</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>HTML Encrypter &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
                            
                            <form method="post" class="form-horizontal form-bordered">
                            
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">HTML Encrypter</h2>
									<p class="panel-subtitle">Allows you to protect your HTML code from theft.</p>
								</header>
								<div class="panel-body">
                                <fieldset>
<?php
@$_SESSION['htmltext-session'] = $_POST['htmltext'];
?>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="htmltext" rows="10" type="text" required><?php
echo $_SESSION['htmltext-session'];
?></textarea>
                                        
<?php
if (isset($_POST['encrypt'])) {
    
    $htmltext = $_POST['htmltext'];
    $a        = "";
    $b        = "";
    for ($i = 0; $i < strlen($htmltext); $i++) {
        $a = (string) dechex(ord($htmltext[$i]));
        switch (strlen($a)) {
            case 1:
                $b .= "\\u000" . $a;
                break;
            case 2:
                $b .= "\\u00" . $a;
                break;
            case 3:
                $b .= "\\u0" . $a;
                break;
            case 4:
                $b .= "\\u" . $a;
                break;
            default:
        }
    }
    $encrypted = "
<script type=\"text/javascript\">
<!-- HTML Encryption provided by BestSecurity -->
<!--
document.write('{$b}')
//-->
</script>
";
    
    echo '<br /><br />
<b>Encrypted HTML Code:</b>
<textarea class="form-control" name="htmltext-encrypted" rows="10" type="text" readonly>' . $encrypted . '</textarea>
</script>
';
    
}
?>     
                                    </div>
                                    <div class="col-md-4">
                                    <p>
                                        <ol>
                                        <li>Enter the HTML code you want to encrypt.
                                        <li><b>Encryption</b> press the button and   <b>Copy the HTML code </b> put on website</li>
                                        </ol>
                                    </p>
                                    </div>
                                    
                                </fieldset>
								</div>
							</section>
                                
                            <input class="btn btn-primary" type="submit" name="encrypt" value="Encrypt">
                                
                            </form>

						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">tips and information</h2>
								</header>
								<div class="panel-body">
									<b>HTML encryption</b> converting website content to an unintelligible format.
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title"> HTML coding comment</h2>
								</header>
								<div class="panel-body">
HTML has the ability to edit symbols.
<br /><br />

								</div>
							</section>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>