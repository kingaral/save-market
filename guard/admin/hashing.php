<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Hashing</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>Hash &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Hash code</h2>
									<p class="panel-subtitle">Hashing</p>
								</header>
								<div class="panel-body">
<?php
@$_SESSION['string-input'] = $_POST['string'];
?>
                                    <form action="" method="post">
											<div class="form-group">
												<label>Text / String:</label>
													<textarea class="form-control" rows="3" name="string" required><?php
echo $_SESSION['string-input'];
?></textarea>
											</div>
                                        <br />
                                        <button type="submit" name="generate" class="btn btn-primary"><i class="fa fa-refresh"></i> Generate hash</button>
								    </form>
                                    
                                    <br /><br />
                                    
                            <div class="tabs">
				                 <ul class="nav nav-tabs">
									<li class="active">
										<a href="#md5" data-toggle="tab">MD5</a>
									</li>
                                    <li>
										<a href="#base64" data-toggle="tab">Base64</a>
									</li>
									<li>
										<a href="#sha-1" data-toggle="tab">SHA-1</a>
									</li>
                                    <li>
										<a href="#sha-256" data-toggle="tab">SHA-256</a>
									</li>
                                    <li>
										<a href="#sha-512" data-toggle="tab">SHA-512</a>
									</li>
                                    <li>
										<a href="#whirlpool" data-toggle="tab">Whirlpool</a>
									</li>
                                     <li>
										<a href="#crypt" data-toggle="tab">Crypt</a>
									</li>
                                    <li>
										<a href="#md2" data-toggle="tab">MD2</a>
									</li>
                                    <li>
										<a href="#md4" data-toggle="tab">MD4</a>
									</li>
                                    <li>
										<a href="#crc32" data-toggle="tab">CRC32</a>
									</li>
                                    <li>
										<a href="#gost" data-toggle="tab">Gost</a>
									</li>
                                    <li>
										<a href="#snefru" data-toggle="tab">Snefru</a>
									</li>
								</ul>
								<div class="tab-content">
<?php
if (isset($_POST['generate'])) {
    $string = $_POST['string'];
    echo '
									<div id="md5" class="tab-pane active">
                                        Generated MD5 Hash:<br />
										<input value="' . md5($string) . '" name="md5" class="form-control" disabled>
									</div>
									<div id="base64" class="tab-pane">
                                        Generated Base64 Hash:<br />
										<input value="' . base64_encode($string) . '" name="base64" class="form-control" disabled>
									</div>
                                    <div id="sha-1" class="tab-pane">
                                        Generated SHA-1 Hash:<br />
										<input value="' . hash('sha1', $string) . '" name="sha-1" class="form-control" disabled>
									</div>
                                    <div id="sha-256" class="tab-pane">
                                        Generated SHA-256 Hash:<br />
										<input value="' . hash('sha256', $string) . '" name="sha-256" class="form-control" disabled>
									</div>
                                    <div id="sha-512" class="tab-pane">
                                        Generated SHA-512 Hash:<br />
										<input value="' . hash('sha512', $string) . '" name="sha-512" class="form-control" disabled>
									</div>
                                    <div id="whirlpool" class="tab-pane">
                                        Generated Whirlpool Hash:<br />
										<input value="' . hash('Whirlpool', $string) . '" name="whirlpool" class="form-control" disabled>
									</div>
                                    <div id="crypt" class="tab-pane">
                                        Generated Crypt Hash:<br />
										<input value="' . crypt($string) . '" name="crypt" class="form-control" disabled>
									</div>
                                    <div id="md2" class="tab-pane">
                                        Generated MD2 Hash:<br />
										<input value="' . hash('md2', $string) . '" name="md2" class="form-control" disabled>
									</div>
                                    <div id="md4" class="tab-pane">
                                        Generated MD4 Hash:<br />
										<input value="' . hash('md4', $string) . '" name="md4" class="form-control" disabled>
									</div>
                                    <div id="crc32" class="tab-pane">
                                        Generated CRC32 Hash:<br />
										<input value="' . hash('crc32', $string) . '" name="crc32" class="form-control" disabled>
									</div>
                                    <div id="gost" class="tab-pane">
                                        Generated Gost Hash:<br />
										<input value="' . hash('gost', $string) . '" name="gost" class="form-control" disabled>
									</div>
                                    <div id="snefru" class="tab-pane">
                                        Generated Gost Hash:<br />
										<input value="' . hash('snefru', $string) . '" name="snefru" class="form-control" disabled>
									</div>
';
} else {
    echo '
									<div id="md5" class="tab-pane active">
                                        Generated MD5 Hash:<br />
										<input value="" name="md5" class="form-control" disabled>
									</div>
									<div id="base64" class="tab-pane">
                                        Generated Base64 Hash:<br />
										<input value="" name="base64" class="form-control" disabled>
									</div>
                                    <div id="sha-1" class="tab-pane">
                                        Generated SHA-1 Hash:<br />
										<input value="" name="sha-1" class="form-control" disabled>
									</div>
                                    <div id="sha-256" class="tab-pane">
                                        Generated SHA-256 Hash:<br />
										<input value="" name="sha-256" class="form-control" disabled>
									</div>
                                    <div id="sha-512" class="tab-pane">
                                        Generated SHA-512 Hash:<br />
										<input value="" name="sha-512" class="form-control" disabled>
									</div>
                                    <div id="whirlpool" class="tab-pane">
                                        Generated Whirlpool Hash:<br />
										<input value="" name="whirlpool" class="form-control" disabled>
									</div>
                                    <div id="crypt" class="tab-pane">
                                        Generated Crypt Hash:<br />
										<input value="" name="crypt" class="form-control" disabled>
									</div>
                                    <div id="md2" class="tab-pane">
                                        Generated MD2 Hash:<br />
										<input value="" name="md2" class="form-control" disabled>
									</div>
                                    <div id="md4" class="tab-pane">
                                        Generated MD4 Hash:<br />
										<input value="" name="md4" class="form-control" disabled>
									</div>
                                    <div id="crc32" class="tab-pane">
                                        Generated CRC32 Hash:<br />
										<input value="" name="crc32" class="form-control" disabled>
									</div>
                                    <div id="gost" class="tab-pane">
                                        Generated Gost Hash:<br />
										<input value="" name="gost" class="form-control" disabled>
									</div>
                                    <div id="snefru" class="tab-pane">
                                        Generated Gost Hash:<br />
										<input value="" name="snefru" class="form-control" disabled>
									</div>
';
}
?>
								</div>
							</div>
                                    
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">List of hash functions</h2>
									<p class="panel-subtitle">A cryptographic list of hash functions.</p>
								</header>
								<div class="panel-body">
									
                                             <table class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>Name</th>
														<th>Length</th>
														<th>Type</th>
													</tr>
												</thead>
												<tbody>
<tr>
<td>BLAKE-256</td>
<td>256 bit</td>
<td>HAIFA structure</td>
</tr>
<tr>
<td>BLAKE-512</td>
<td>512 bit</td>
<td>HAIFA structure</td>
</tr>
<tr>
<td>ECOH</td>
<td>224 bit from 512 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>FSB</td>
<td>160 bit from 512 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>GOST</td>
<td>256 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>Grøstl</td>
<td>256  bit from 512 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>HAS-160</td>
<td>160 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>HAVAL</td>
<td>128 bit from 256 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>JH</td>
<td>512 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>MD2</td>
<td>128 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>MD4</td>
<td>128 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>MD5</td>
<td>128 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>MD6</td>
<td>512 bit</td>
<td>Merkle tree NLFSR</td>
</tr>
<tr>
<td>RadioGatún</td>
<td>Up to 1216 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>RIPEMD-64</td>
<td>64 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>RIPEMD-160</td>
<td>160 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>RIPEMD-320</td>
<td>320 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>SHA-1</td>
<td>160 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>SHA-224</td>
<td>224 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>SHA-256</td>
<td>256 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>SHA-384</td>
<td>384 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>SHA-512</td>
<td>512 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>SHA-3</td>
<td>arbitrary</td>
<td>Sponge functions</td>
</tr>
<tr>
<td>Skein</td>
<td>arbitrary</td>
<td>A unique barrier iteration</td>
</tr>
<tr>
<td>SipHash</td>
<td>64 bit</td>
<td>PRF with endurance</td>
</tr>
<tr>
<td>Snefru</td>
<td>128 or 256 bit</td>
<td>хэш</td>
</tr>Hash
<tr>
<td>Spectral Hash</td>
<td>512 bit</td>
<td>Wide Pipe Merkle-Damgård structure</td>
</tr>
<tr>
<td>SWIFFT</td>
<td>512 bit</td>
<td>Hash</td>
</tr>
<tr>
<td>Tiger</td>
<td>192 bit</td>
<td>Merkle-Damgård structure</td>
</tr>
<tr>
<td>Whirlpool</td>
<td>512 bit</td>
<td>Hash</td>
</tr>
												</tbody>
											</table>
                                    
								</div>
                                    
							</section>

						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Tips and Information</h2>
								</header>
								<div class="panel-body">
								Obtaining the value of a hash function to allow or secure data. A hash is generated from a text string. </div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Objectives</h2>
								</header>
								<div class="panel-body">
								Hash can be used for different purposes:
                                    <ol>
                                    <li>
									can be used to compare large amounts of data. You create a hash for the data, if you want to compare the data you can just compare the hashes.
								  </li>
                                    <li>
                                    A hash allows indexing of data. For example, you create hash tables to refer to the desired row.
                                    </li>
                                    <li>
                                    Hashes can be used in cryptographic applications such as electronic signatures.
                                    </li>
                                    
                                    </ol>
								</div>
							</section>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>