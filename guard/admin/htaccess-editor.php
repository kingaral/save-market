<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>.htaccess editor</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>.htaccess editor &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>
<?php
$htaccess = $_SERVER['DOCUMENT_ROOT'] . "/.htaccess";
if (!file_exists($htaccess)) {
    echo '<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				No created <strong>.htaccess</strong> file on your site and will now be created in the site\'s root folder - <strong>' . $htaccess . '</strong> .
          </div>';
    $content = "";
    $fp      = fopen($htaccess, "wb");
    fwrite($fp, $content);
    fclose($fp);
}
?>
					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
                            
                            <form method="post" class="form-horizontal form-bordered">
                            
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">.htaccess editor</h2>
									<p class="panel-subtitle">allows you to edit your website's .htaccess file.</p>
								</header>
								<div class="panel-body">
                                <fieldset>
                                    
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="htaccess" rows="25" type="text"><?php
$htaccess = $_SERVER['DOCUMENT_ROOT'] . "/.htaccess";
@$fh = fopen($htaccess, 'r');
while (@$line = fgets($fh)) {
    echo (@$line);
}
@fclose($fh);
?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                    <p>Take care to ensure that errors do not make your website unavailable.</p>
                                     <ul class="description">
                                         <li><a href="http://www.google.com/search?q=htaccess+tutorial" title="Search for htaccess tutorials" target="_blank">
                                             <img width="16px" src="http://google.com/favicon.ico" alt="google favicon"> htaccess tutorial</a>
                                         </li>
                                         <li><a href="http://httpd.apache.org/docs/current/howto/htaccess.html" title="Read about htaccess at apache.org" target="_blank">
                                             <img width="16px" src="http://apache.org/favicon.ico" alt="apache favicon"> htaccess</a>
                                         </li>
                                         <li><a href="http://httpd.apache.org/docs/current/mod/mod_rewrite.html" title="Read about mod_rewrite at apache.org" target="_blank">
                                             <img width="16px" src="http://apache.org/favicon.ico" alt="apache favicon"> mod_rewrite</a>
                                         </li>
                                     </ul>
                                    </div>
                                    
                                </fieldset>
								</div>
							</section>
                                
                            <input class="btn btn-primary" type="submit" name="ht-edit" value="Save all changes">
                                
                            </form>
                            
<?php
if (isset($_POST['ht-edit'])) {
    
    $fn = $_SERVER['DOCUMENT_ROOT'] . "/.htaccess";
    @$file = fopen($fn, "w+");
    fwrite($file, $_POST['htaccess']);
    fclose($file);
    
    echo '<script type="text/javascript">window.location = "htaccess-editor"</script>';
    
}
?>

						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Tips and information</h2>
								</header>
								<div class="panel-body">
								An .htaccess file is a configuration file in the directory of several web servers. They should be located within the web paragraph.
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">In common use</h2>
								</header>
								<div class="panel-body">
<ul>
<li><b>Authorization, authentication</b></li>
The .htaccess file is used to specify the security level for directories. Along with the htaccess file is the .htpasswd file, which stores users and their passwords.
<li><b>URL rewriting</b></li>
Servers often use .htaccess to rewrite long, overly large addresses into shorter, more memorable addresses.
<li><b>Lock up</b></li>
Use ban or allow to block users' IP addresses and domains.
<li><b>SSI</b></li>
Includes server enablement.
<li><b>List of directories</b></li>
Server behavior when a web page is not displayed.
    <li><b>Error messages</b></li>
	Changing the web page, for example HTTP 404 Not Found if there is an error from the server, or HTTP 301 Moved if the page is being submitted to the search engine.
<li><b>MIME types</b></li>
write to the server handling different types of files.
<li><b>Cache control</b></li>
.htaccess files are used by web browsers to control caching to reduce bandwidth usage.
</ul>
								</div>
							</section>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>