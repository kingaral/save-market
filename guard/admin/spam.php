<?php
include "core.php";
head();
?>
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Secure module</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard">
										<i class="fa fa-home"></i>
									</a>
								</li>
                                <li><span>Secure module &nbsp;&nbsp;&nbsp;</span></li>
							</ol>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-9">
<?php
$table = $prefix . 'spam-settings';
@$query = mysqli_query($connect, "SELECT * FROM `$table`");
@$row = mysqli_fetch_array($query);
if ($row['protection'] == 'Yes') {
    echo '
                                                     <section class="panel panel-success">
';
} else {
    echo '
                                                     <section class="panel panel-danger">
';
}
?>
							
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Spam - Secure module</h2>
								</header>
								<div class="panel-body">
<?php
if ($row['protection'] == 'Yes') {
    echo '
<div class="jumbotron">
        <h1 style="color: #47A447;"><i class="fa fa-check-circle-o"></i> Enabled</h1>
        <p>The site is protected from <b>Spammers</b></p>
</div>
';
} else {
    echo '
<div class="jumbotron">
        <h1 style="color: #d2322d;"><i class="fa fa-times-circle-o"></i> Disabled</h1>
        <p>The site is not protected from <b>Spammers</b></p>
</div>
';
}
?>

								</div>
							</section>

                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Spam database (DNSBL)</h2>
                                    <p class="panel-subtitle">The spam database is needed to check whether the user is on the spammers' list.</p>
								</header>
								<div class="panel-body">
                                    
                    <center><a href="#add" class="mb-xs mt-xs mr-xs modal-with-zoom-anim btn btn-primary"><i class="fa fa-plus-circle"></i> Add spam database (DNSBL)</a></center>
                                    
                                    
                                    <div id="add" class="zoom-anim-dialog modal-block modal-header-color modal-block-primary mfp-hide">
										<section class="panel">
											<header class="panel-heading">
												<h2 class="panel-title">Add spam database (DNSBL)</h2>
											</header>
											<div class="panel-body">
                                                <form class="form-horizontal mb-lg" method="POST">
												<div class="form-group">
                                                        <label class="col-sm-3 control-label">Add spam database (DNSBL):</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="database" value="" required/>
														</div>
												</div>
                                                
											</div>
											<footer class="panel-footer">
												<div class="row">
													<div class="row">
													<div class="col-md-8 text-left">
									                &nbsp;&nbsp;&nbsp; <input class="btn btn-primary" name="add-database" type="submit" value="Add">
													</div>
                                                    </form>
                                                    <div class="col-md-4 text-right">
														<button class="btn btn-default modal-dismiss">Close</button> &nbsp;&nbsp;
													</div>
												</div>
												</div>
											</footer>
										</section>
									</div>
                                    </form>
                                    
<?php
if (isset($_POST['add-database'])) {
    $table      = $prefix . 'dnsbl-databases';
    $database   = $_POST['database'];
    $queryvalid = mysqli_query($connect, "SELECT * FROM `$table` WHERE `database`='$database' LIMIT 1");
    $validator  = mysqli_num_rows($queryvalid);
    if ($validator > "0") {
        echo '<meta http-equiv="refresh" content="0;url=spam.php">';
    } else {
        $query = mysqli_query($connect, "INSERT INTO `$table` (`database`) VALUES ('$database')");
        echo '<meta http-equiv="refresh" content="0;url=spam.php">';
    }
}
?>
                                    
<table class="table table-bordered table-striped table-hover mb-none" id="datatable-default">
									<thead>
										<tr>
											<th>Spam database</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
<?php
$table = $prefix . 'dnsbl-databases';
$query = mysqli_query($connect, "SELECT * FROM `$table`");
while ($row = mysqli_fetch_assoc($query)) {
    echo '
										<tr>
                                            <td>' . $row['database'] . '</td>
											<td>
                                            <a href="?delete-id=' . $row['id'] . '" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
											</td>
										</tr>
';
}

if (isset($_GET['delete-id'])) {
    $id    = (int) $_GET["delete-id"];
    $table = $prefix . 'dnsbl-databases';
    $query = mysqli_query($connect, "DELETE FROM `$table` WHERE id='$id'");
    echo "<meta http-equiv=Refresh content=0;url=spam.php>";
}
?>
									</tbody>
								</table>
                                
                                <br />
                                <div class="alert alert-info">
								        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										<strong>Do not add more than 3 blacklists because your website will simply load.</strong>
								</div>
                                
								</div>
							</section>
						</div>
						<div class="col-md-3">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">What is the spam?</h2>
								</header>
								<div class="panel-body">
								<b>Electronic Spamming</b> is the use of an electronic system of messages, spam and advertisements. 
								</div>
							</section>
                            
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">What is DNSBL?</h2>
								</header>
								<div class="panel-body">
								A<b>DNS Blackhole List shows a list of IP addresses that are often used to send spam.
									 <br /><br />
                                    
									 All <b>Blacklists</b> are located here: <b><a href="http://www.dnsbl.info/dnsbl-list.php" target="_blank">http://www. dnsbl.info/dnsbl-list.php</a></b>
								</div>
							</section>
                            
<form class="form-horizontal form-bordered" action="" method="post">
                            <section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">Settings</h2>
								</header>
								<div class="panel-body">
                                    <div class="form-group">
											<label class="col-sm-4 control-label">Security: </label>
											<div class="col-sm-8">
												<div class="switch switch-success">
														<input type="checkbox" name="protection" data-plugin-ios-switch 
<?php
$table = $prefix . 'spam-settings';
@$query = mysqli_query($connect, "SELECT * FROM `$table`");
@$row = mysqli_fetch_array($query);
if ($row['protection'] == 'Yes') {
    echo 'checked="checked" checked';
}
?>
                                                         value="On" />
												</div>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-4 control-label">Logging: </label>
											<div class="col-sm-8">
												<div class="switch switch-success">
														<input type="checkbox" name="logging" data-plugin-ios-switch 
<?php
if ($row['logging'] == 'Yes') {
    echo 'checked="checked" checked';
}
?>
                                                               value="On" />
												</div>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-4 control-label">Autoban: </label>
											<div class="col-sm-8">
                                                <div class="switch switch-success">
														<input type="checkbox" name="autoban" data-plugin-ios-switch 
<?php
if ($row['autoban'] == 'Yes') {
    echo 'checked="checked" checked';
}
?>
                                                               value="On" />
												</div>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-4 control-label">Mail notifications: </label>
											<div class="col-sm-8">
                                                <div class="switch switch-success">
														<input type="checkbox" name="mail" data-plugin-ios-switch 
<?php
if ($row['mail'] == 'Yes') {
    echo 'checked="checked" checked';
}
?>
                                                               value="On" />
												</div>
											</div>
										</div>
                                        <div class="form-group">
											<label class="col-sm-4 control-label">URL redirect: </label>
											<div class="col-sm-8">
												<input name="redirect" class="form-control" type="text" value="<?php
echo $row['redirect'];
?>" required>
											</div>
										</div>
									</div>
									<footer class="panel-footer">
										<button class="btn btn-primary" name="save" type="submit">Save</button>
										<button type="reset" class="btn btn-default">Clean</button>
									</footer>
							</section>
</form>
<?php
if (isset($_POST['save'])) {
    $table = $prefix . 'spam-settings';
    
    if (isset($_POST['protection'])) {
        $protection = 'Yes';
    } else {
        $protection = 'No';
    }
    
    if (isset($_POST['logging'])) {
        $logging = 'Yes';
    } else {
        $logging = 'No';
    }
    
    if (isset($_POST['autoban'])) {
        $autoban = 'Yes';
    } else {
        $autoban = 'No';
    }
    
    if (isset($_POST['mail'])) {
        $mail = 'Yes';
    } else {
        $mail = 'No';
    }
    
    $redirect = $_POST['redirect'];
    
    $query = mysqli_query($connect, "UPDATE `$table` SET protection='$protection', logging='$logging', autoban='$autoban', mail='$mail', redirect='$redirect' WHERE id=1");
    echo '<meta http-equiv="refresh" content="0;url=spam.php">';
}
?>

						</div>
					</div>
					<!-- end: page -->
				</section>
<?php
footer();
?>