<?php
include "../config.php";

session_start();
$_SESSION = array();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>SecurirySystem</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="../assets/img/favicon.ico">
    <meta charset="utf-8">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" media="screen">
	<script src="../assets/js/jquery-1.11.0.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../js/html5.js"></script>
      <script src="../js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="container">