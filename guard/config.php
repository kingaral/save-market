<?php
$host     = "172.16.238.12"; // Database Host
$database = "security"; // Database Name
$user     = "admin"; // Database Username
$password = "admin"; // Database's user Password
$prefix   = "security_"; // Database Prefix for the script tables

$connect = mysqli_connect($host,$user,$password,$database);

// Checking Connection
if (mysqli_connect_errno())
{
  echo "Failed to connect with MySQL: " . mysqli_connect_error();
}

mysqli_set_charset($connect, "utf8");

// $site_url      = "https://security-2.zilla.kz";

$site_url      = "http://htdocs.dvl.to";
$phpguard_path = "http://htdocs.dvl.to/guard";

$version = "3.2";
?>