-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 11 2021 г., 17:00
-- Версия сервера: 5.6.41
-- Версия PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `security-system`
--

-- --------------------------------------------------------

--
-- Структура таблицы `security_bans`
--

CREATE TABLE `security_bans` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `until` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `security_bans-country`
--

CREATE TABLE `security_bans-country` (
  `id` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Banned countries table';

-- --------------------------------------------------------

--
-- Структура таблицы `security_content-protection`
--

CREATE TABLE `security_content-protection` (
  `id` int(11) NOT NULL,
  `function` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `alert` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_content-protection`
--

INSERT INTO `security_content-protection` (`id`, `function`, `enabled`, `alert`, `message`) VALUES
(1, 'rightclick', 'No', 'Yes', 'Context Menu not allowed'),
(2, 'rightclick_images', 'No', 'Yes', 'Context Menu on Images not allowed'),
(3, 'cut', 'No', 'Yes', 'Cut not allowed'),
(4, 'copy', 'No', 'Yes', 'Copy not allowed'),
(5, 'paste', 'No', 'Yes', 'Paste not allowed'),
(6, 'drag', 'No', 'No', ''),
(7, 'drop', 'No', 'No', ''),
(8, 'printscreen', 'No', 'Yes', 'It is not allowed to use the Print Screen button'),
(9, 'print', 'No', 'Yes', 'It is not allowed to Print'),
(10, 'view_source', 'No', 'Yes', 'It is not allowed to view the source code of the site'),
(11, 'offline_mode', 'No', 'Yes', 'You have no access to save the page'),
(12, 'iframe_out', 'No', 'No', ''),
(13, 'exit_confirmation', 'No', 'Yes', 'Do you really want to exit our website?'),
(14, 'selecting', 'No', 'No', '');

-- --------------------------------------------------------

--
-- Структура таблицы `security_dnsbl-databases`
--

CREATE TABLE `security_dnsbl-databases` (
  `id` int(11) NOT NULL,
  `database` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_dnsbl-databases`
--

INSERT INTO `security_dnsbl-databases` (`id`, `database`) VALUES
(1, 'dnsbl.sorbs.net'),
(2, 'zen.spamhaus.org');

-- --------------------------------------------------------

--
-- Структура таблицы `security_ip-whitelist`
--

CREATE TABLE `security_ip-whitelist` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `security_logs`
--

CREATE TABLE `security_logs` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `browser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `browser_version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `os` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `os_version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referer_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_logs`
--

INSERT INTO `security_logs` (`id`, `ip`, `date`, `time`, `page`, `type`, `autoban`, `browser`, `browser_version`, `os`, `os_version`, `referer_url`) VALUES
(6, '127.0.0.1', '11 April 2021', '16:20', '/index.php', 'Proxy', 'No', 'Google Chrome', '89.0.4389.114', 'Windows', '10 (x64)', ''),
(7, '127.0.0.1', '11 April 2021', '16:52', '', 'SQLi', 'No', 'Google Chrome', '89.0.4389.114', 'Windows', '10 (x64)', '');

-- --------------------------------------------------------

--
-- Структура таблицы `security_malwarescanner-settings`
--

CREATE TABLE `security_malwarescanner-settings` (
  `id` int(11) NOT NULL,
  `file-extensions` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'php|php3|php4|php5|phps|htm|html|htaccess|js',
  `ignored-dirs` text COLLATE utf8_unicode_ci NOT NULL,
  `scan-dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '../../'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_malwarescanner-settings`
--

INSERT INTO `security_malwarescanner-settings` (`id`, `file-extensions`, `ignored-dirs`, `scan-dir`) VALUES
(1, 'php|phtml|php3|php4|php5|phps|htaccess|txt|gif', '.|..|.DS_Store|.svn|.git', '../../');

-- --------------------------------------------------------

--
-- Структура таблицы `security_massrequests-settings`
--

CREATE TABLE `security_massrequests-settings` (
  `id` int(11) NOT NULL,
  `protection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `logging` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pages/mass-requests.php',
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.3'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_massrequests-settings`
--

INSERT INTO `security_massrequests-settings` (`id`, `protection`, `logging`, `autoban`, `redirect`, `mail`, `time`) VALUES
(1, 'No', 'Yes', 'No', 'pages/mass-requests.php', 'No', '0.5');

-- --------------------------------------------------------

--
-- Структура таблицы `security_pages-layolt`
--

CREATE TABLE `security_pages-layolt` (
  `id` int(11) NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_pages-layolt`
--

INSERT INTO `security_pages-layolt` (`id`, `page`, `text`, `image`) VALUES
(1, 'Banned', 'You are banned from the site', 'https://www.parismou.org/sites/default/files/Banned.jpg'),
(2, 'Blocked', 'Your attack was detected', 'https://www.securitylab.ru/upload/iblock/738/73828a4046f231d23c1f974bf303a498.jpg'),
(3, 'Mass_Requests', 'Attention, too many connections', 'https://vps.ua/blog/wp-content/uploads/2018/08/vps-ddos-1.jpg'),
(4, 'Proxy', 'You are using a Proxy or you are listed in the Blacklist of Proxies', 'https://nosok.org/images/scr/nosok-proxy.png'),
(5, 'Spam', 'You are in the Blacklist of Spammers and you can not continue to the site', 'https://toppng.com/uploads/preview/no-spam-icon-custom-icons-footprint-spam-icon-design-icon-spam-11553378799uqjfupfpp4.png'),
(6, 'Banned_Country', 'Sorry, but your country is banned from the site and you can not continue', 'https://img.icons8.com/plasticine/2x/country.png');

-- --------------------------------------------------------

--
-- Структура таблицы `security_proxy-list`
--

CREATE TABLE `security_proxy-list` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `port` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `security_proxy-settings`
--

CREATE TABLE `security_proxy-settings` (
  `id` int(11) NOT NULL,
  `protection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `logging` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pages/proxy.php',
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_proxy-settings`
--

INSERT INTO `security_proxy-settings` (`id`, `protection`, `logging`, `autoban`, `redirect`, `mail`) VALUES
(1, 'No', 'Yes', 'No', 'pages/proxy.php', 'No');

-- --------------------------------------------------------

--
-- Структура таблицы `security_settings`
--

CREATE TABLE `security_settings` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail_notifications` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `realtime_protection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='All BestSecurity settings will be stored here.';

--
-- Дамп данных таблицы `security_settings`
--

INSERT INTO `security_settings` (`id`, `email`, `mail_notifications`, `realtime_protection`) VALUES
(1, 'admin@mail.com', 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Структура таблицы `security_spam-settings`
--

CREATE TABLE `security_spam-settings` (
  `id` int(11) NOT NULL,
  `protection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `logging` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pages/spammer.php',
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_spam-settings`
--

INSERT INTO `security_spam-settings` (`id`, `protection`, `logging`, `redirect`, `autoban`, `mail`) VALUES
(1, 'Yes', 'Yes', 'pages/spammer.php', 'No', 'No');

-- --------------------------------------------------------

--
-- Структура таблицы `security_sqli-patterns`
--

CREATE TABLE `security_sqli-patterns` (
  `id` int(11) NOT NULL,
  `pattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_sqli-patterns`
--

INSERT INTO `security_sqli-patterns` (`id`, `pattern`) VALUES
(1, 'union'),
(2, 'cookie'),
(3, 'coockie'),
(4, 'concat'),
(5, 'table'),
(6, 'from'),
(7, 'where'),
(8, 'exec'),
(9, 'shell'),
(10, 'wget'),
(11, '/**/'),
(12, '0x3a'),
(13, 'null'),
(14, 'BUN'),
(15, 'S@BUN'),
(16, 'char'),
(17, '\'%'),
(18, 'OR%');

-- --------------------------------------------------------

--
-- Структура таблицы `security_sqli-settings`
--

CREATE TABLE `security_sqli-settings` (
  `id` int(11) NOT NULL,
  `protection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `logging` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `redirect` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pages/blocked.php',
  `autoban` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_sqli-settings`
--

INSERT INTO `security_sqli-settings` (`id`, `protection`, `logging`, `redirect`, `autoban`, `mail`) VALUES
(1, 'Yes', 'Yes', 'pages/blocked.php', 'No', 'No');

-- --------------------------------------------------------

--
-- Структура таблицы `security_users`
--

CREATE TABLE `security_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'johndoe@mail.com',
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'assets/images/avatars/no-avatar.png'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_users`
--

INSERT INTO `security_users` (`id`, `username`, `password`, `email`, `avatar`) VALUES
(1, 'admin', 'YWRtaW4=', 'admin@mail.com', 'assets/images/avatars/no-avatar.png');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `security_bans`
--
ALTER TABLE `security_bans`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_bans-country`
--
ALTER TABLE `security_bans-country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_content-protection`
--
ALTER TABLE `security_content-protection`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_dnsbl-databases`
--
ALTER TABLE `security_dnsbl-databases`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_ip-whitelist`
--
ALTER TABLE `security_ip-whitelist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_logs`
--
ALTER TABLE `security_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_malwarescanner-settings`
--
ALTER TABLE `security_malwarescanner-settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_massrequests-settings`
--
ALTER TABLE `security_massrequests-settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_pages-layolt`
--
ALTER TABLE `security_pages-layolt`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_proxy-list`
--
ALTER TABLE `security_proxy-list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_proxy-settings`
--
ALTER TABLE `security_proxy-settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_settings`
--
ALTER TABLE `security_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_spam-settings`
--
ALTER TABLE `security_spam-settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_sqli-patterns`
--
ALTER TABLE `security_sqli-patterns`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_sqli-settings`
--
ALTER TABLE `security_sqli-settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_users`
--
ALTER TABLE `security_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `security_bans`
--
ALTER TABLE `security_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `security_bans-country`
--
ALTER TABLE `security_bans-country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `security_content-protection`
--
ALTER TABLE `security_content-protection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `security_dnsbl-databases`
--
ALTER TABLE `security_dnsbl-databases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `security_ip-whitelist`
--
ALTER TABLE `security_ip-whitelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `security_logs`
--
ALTER TABLE `security_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `security_malwarescanner-settings`
--
ALTER TABLE `security_malwarescanner-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_massrequests-settings`
--
ALTER TABLE `security_massrequests-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_pages-layolt`
--
ALTER TABLE `security_pages-layolt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `security_proxy-list`
--
ALTER TABLE `security_proxy-list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `security_proxy-settings`
--
ALTER TABLE `security_proxy-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_settings`
--
ALTER TABLE `security_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_spam-settings`
--
ALTER TABLE `security_spam-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_sqli-patterns`
--
ALTER TABLE `security_sqli-patterns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `security_sqli-settings`
--
ALTER TABLE `security_sqli-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `security_users`
--
ALTER TABLE `security_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
