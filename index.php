<?php
	session_start();
	require_once "guard/config.php";
	require_once "guard/security.php";

    require_once "controller/start.php";

    require_once "view/head.php";

    if ($myPage != '') {
		include "view/$myPage.php";
	} else {
		include "view/main.php";
	}

    require_once "view/foot.php";
?>