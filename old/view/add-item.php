<script type="text/javascript" src="/assets/js/add-item.js"></script>

<legend>Add announcment</legend>

<form action="" method="post" class="row" enctype="multipart/form-data">

    <div class="col-md-6">
        <div class="form-group">
            <label for="title">Name</label>
            <input type="text" class="form-control" name="title" id="title" required>
        </div>
        <div class="form-group">
            <label for="cat">Category</label>
            <select class="form-control" name="cat" id="cat">
                <?php foreach (getCats() as $cat) { ?><option value="<?=$cat['id']?>"><?=$cat['title']?></option><?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="info">Descriotion</label>
            <textarea class="form-control" name="info" id="info" rows="5" required></textarea>
        </div>
        <div class="form-group">
            <label for="city">City</label>
            <select name="city" id="city" class="form-control">
                <option value="Almaty">Almaty</option>
                <option value="Taldykorgan">Taldykorgan</option>
                <option value="Astsana">Astsana</option>
                <option value="Semey">Semey</option>
                <option value="Aktobe">Aktobe</option>
            </select>
        </div>
        <div class="form-group">
            <label for="reward">Cost:</label>
            <input type="text" class="form-control" name="reward" id="reward" required>
        </div>
        <div class="form-group">
            <label for="file">Attach photo:</label>
            <input type="file" id="file" name="photo" class="form-control">
        </div>
    </div>



    <div class="col-md-6">
        <div class="form-group">
            <label for="find-place">See map:</label>
            <div id="map"></div>
        </div>
        <div class="form-group">
            <label for="coors">Coordinats</label>
            <input type="text" class="form-control" name="coors" id="coors" required>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-block btn-success" name="add-item-btn" value="Add announcment">
        </div>
    </div>


</form>