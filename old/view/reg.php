<legend>Sign Up</legend>

<form action="" method="post" style="width: 400px; margin: 0 auto;">
    <div class="form-group">
        <label for="fio">Name</label>
        <input type="text" class="form-control" id="fio" name="fio" required>
    </div>
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" required>
    </div>
    <div class="form-group">
        <label for="password1">Password</label>
        <input type="password" class="form-control" id="password1" name="password1" required>
    </div>
    <div class="form-group">
        <label for="password2">Password confirmation</label>
        <input type="password" class="form-control" id="password2" name="password2" required>
    </div>
    <div class="form-group">
        <input type="submit" name="add-user" value="Submit" class="btn btn-block btn-default">
    </div>
</form>