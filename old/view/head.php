<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LOGO</title>
    <!--CSS Styles-->
    <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css">
    <!--<link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap-theme.css">-->
    <link rel="stylesheet" href="/assets/css/main.css">
    <!--JavaScripts-->
    <script src="/assets/jquery/dist/jquery.min.js"></script>
    <script src="/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>





<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">LOGO</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-left">
                <li><a href="/">Main menu </a></li>
                <li><a href="?page=contact">FAQ</a></li>
                <li><a href="?page=search">Search</a></li>
                <?php if (!$user) { ?>
                <li><a href="?page=reg">Sign UP</a></li>
                <?php } ?>
            </ul>

            <?php if ($user) { ?>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$user['fio']?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?page=cabinet">Profile</a></li>
                        <li><a href="?logout">Exit</a></li>
                    </ul>
                </li>
                <li>
                    <form action="" method="post">
                        <button type="submit" name="add-item-link" style="margin-top: 7px;" class="btn btn-warning">
                            <span class="glyphicon glyphicon-plus"></span> Add Announcements
                        </button>
                    </form>
                </li>
            </ul>
            <?php }else{ ?>
            <form class="navbar-form navbar-right" method="post">
                <div class="form-group">
                    <input name="login" type="text" class="form-control input-sm" placeholder="login">
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control input-sm" placeholder="password">
                </div>
                <button name="auth" type="submit" class="btn btn-default btn-sm">Log in</button>
            </form>
            <?php } ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<div class="container main-content">
    <?=$message?>