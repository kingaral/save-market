
<legend>Feedback</legend>

<p class="alert alert-warning">Feedback to Admninistration</p>

    <form action="" method="post">
        <div class="form-group">
            <label for="title">Theme</label>
            <input type="text" class="form-control" name="title" required>
        </div>

        <div class="form-group">
            <label for="text">Message</label>
            <textarea name="msg" cols="30" rows="10" class="form-control" required></textarea>
        </div>

        <div class="form-group">
            <label for="title">E-mail</label>
            <input type="email" class="form-control" name="email" required>
        </div>

        <div class="form-group">
            <input type="submit" name="btn-contact" class="btn btn-default btn-block" value="Send">
        </div>
    </form>
