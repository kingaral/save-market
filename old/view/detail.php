<script type="text/javascript">
    var myMap;

    // Дождёмся загрузки API и готовности DOM.
    ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('map', {
            center: [<?=$item['coors']?>],
            zoom: 15
        });

        myPlacemark = new ymaps.Placemark([<?=$item['coors']?>]);
        myMap.geoObjects.add(myPlacemark);

    }
</script>

    <div class="row">
        <div class="col-md-8">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="detail-photo"><?=photoItem($item['id'])?></div>
                    <h4><?=$item['title']?></h4>
                    <p style="text-align: justify;"><?=$item['info']?></p>
                    <h4>Map</h4>
                    <p><div id="map" style="height: 200px;"></div></p>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <?php if ($item['user'] != $user['id'] && $user) { ?>
            <form action="" method="post">
                <input type="hidden" name="user" value="<?=$item['user']?>">
                <div class="form-group">
                    <textarea name="msg" required placeholder="If you will have question's, write here" rows="3" class="form-control"></textarea>
                </div>
                <button name="send-msg-item" type="submit" class="btn btn-block btn-success">Send message to Author</button>
            </form>
                <br>
            <?php } ?>


            <div class="panel panel-default">
                <div class="panel-body" style="text-align: center;">
                    <img src="/assets/images/user.png" alt="">
                    <div style="margin-top: 10px;"><?=getUser($item['user'])['fio']?></div>
                </div>
            </div>

            <table class="table table-bordered">
                <tr>
                    <th>Announced date:</th>
                    <td><?=date(FORMAT_DATE, $item['time'])?></td>
                </tr>
                <tr>
                    <th>City:</th>
                    <td><?=$item['city']?></td>
                </tr>
                <tr>
                    <th>Cost:</th>
                    <td><?=$item['reward']?></td>
                </tr>
                <tr>
                    <th>Type:</th>
                    <td><?=($item['type'] == 1) ? 'Sell' : 'Buy' ?></td>
                </tr>
            </table>
        </div>
    </div>
<?php if (nodata($item)) nodata($item); ?>