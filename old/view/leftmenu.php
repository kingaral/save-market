<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body">
            <div style="text-align: center;"><img src="/assets/images/user.png" alt=""></div>
            <h5 style="margin: 0; margin-top: 10px; text-align: center;"><?=$user['fio']?></h5>
        </div>
        <div class="list-group">
            <a href="/?page=cabinet" class="list-group-item"><span class="glyphicon glyphicon-volume-down"></span> &nbsp; Announsments</a>
            <a href="/?page=dialog" class="list-group-item"><span class="glyphicon glyphicon-envelope"></span> &nbsp; Messages</a>
            <a href="/?logout" class="list-group-item"><span class="glyphicon glyphicon-off"></span> &nbsp; Exit</a>
        </div>
    </div>
</div>