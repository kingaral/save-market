
    <div class="row">



        <?php require_once "view/leftmenu.php"; ?>






        <div class="col-md-8">

            <legend>My announcments</legend>

            <div class="panel panel-default">
                <div class="panel-body">

                    <?php foreach (getItemsByUser($user['id']) as $item) { ?>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <?=photoItem($item['id'], 'media-object')?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="?page=detail&id=<?= $item['id'] ?>"><?=$item['title']?></a></h4>
                            <div style="text-align: justify;"><?=mb_substr($item['info'], 0, 90)?>...</div>
                        </div>
                    </div>
                        <hr>
                    <?php } ?>

                    <?php if (count(getItemsByUser($user['id'])) == 0) { ?>
                    <div style="text-align: center;"><img src="/assets/images/marker.png" alt=""></div>
                    <div class="alert alert-warning" style="margin-top: 20px;">
                        <h5 style="margin: 0; text-align: center;">Your announcments are empty</h5>
                    </div>
                    <div style="text-align: center;"><a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span> Add announcments</a></div>
                    <br>
                    <?php } ?>
                </div>
            </div>

        </div>

    </div>
