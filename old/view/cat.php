<legend><?=$cat['title']?></legend>
<table class="last-ads">
    <?php $i = 1; foreach (getItemsByCat($cat['id']) as $item) { ?>
        <?=($i == 1) ? '<tr>' : '' ?>
        <td>
            <a href="?page=detail&id=<?=$item['id']?>" class="ads-link">
                <div class="ads-img"><?=photoItem($item['id'])?></div>
                <div class="ads-title"><?=$item['title']?></div>
            </a>
        </td>
        <?php if ($i == 4) { echo '</tr>'; $i = 0; } ?>
    <?php $i++; } ?>
    <?=nodata(getItemsByCat($cat['id']))?>
</table>