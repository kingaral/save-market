<?php
    require_once "controller/start.php";

    require_once "view/head.php";
    if ($page != '') include "view/$page.php";
    else include "view/main.php";
    require_once "view/foot.php";
?>