<?php

function login($login, $password) {
    $query = "SELECT `id` FROM `users` WHERE `login`='$login' AND `password`='$password'";
    $res = getRow($query);
    if ($res) {
        $_SESSION['userid'] = $res['id'];
        return true;
    }
    return false;
}

function reg($data) {
    if ($data['password1'] != $data['password2']) return false;
    if (getUserByLogin($data['login'])) return false;
    $data['password'] = $data['password1'];
    unset($data['password1']);
    unset($data['password2']);
    if (addRow('users', $data)) return true;
    return false;
}

function getUserByLogin($login) {
    $sql = getRow("SELECT * FROM `users` WHERE `login` = '$login' ");
    if ($sql) return true;
    return false;
}

function getUser($id) {
    if (!is_numeric($id)) exit;
    $query = "SELECT * FROM `users` WHERE `id`=$id";
    return getRow($query);
}

function logout() {
    unset($_SESSION['userid']);
    return true;
}

function nodata($number) {
    if (count($number) == 0) return '<div class="alert alert-warning">Empty</div>';
    return false;
}

function getItem($id) {
    if (!is_numeric($id)) exit('ERROR');
    return getRow("SELECT * FROM `items` WHERE `id` = $id");
}

function getItems($limit = 8) {
    $query = "SELECT * FROM `items` ORDER BY `id` DESC LIMIT $limit";
    return getTable($query);
}

function getCats() {
    return getTable("SELECT * FROM `cats`");
}

function getCat($id) {
    if (!is_numeric($id)) exit('ERROR');
    return getRow("SELECT * FROM `cats` WHERE `id` = $id");
}

function getItemsByUser($user) {
    if (!is_numeric($user)) exit('ERROR');
    $query = "SELECT * FROM `items` WHERE `user` = $user";
    return getTable($query);
}

function getItemsByCat($cat) {
    if (!is_numeric($cat)) exit('ERROR');
    $query = "SELECT * FROM `items` WHERE `cat` = $cat";
    return getTable($query);
}

function addContact($data) {
    return addRow('contact', $data);
}

function addDialog($data) {
    return addRow('dialog', $data);
}

function addItem($data) {
    return addRow('items', $data);
}

function getDialog($author) {
    if (!is_numeric($author)) exit('ERROR');
    $query = "SELECT * FROM `dialog` WHERE `author` = $author";
    return getTable($query);
}

function search($data)
{
    $cat = (isset($data['cat'])) ? "`cat` = {$data['cat']} AND" : false;
    $type = (isset($data['type'])) ? " `type` = '{$data['type']}' AND" : false;
    $city = (isset($data['city'])) ? " `city` = '{$data['city']}' AND" : false;
    $ot = ($data['ot'] != '') ? " `reward` >= {$data['ot']} AND" : false;
    $do = ($data['do'] != '') ? " `reward` <= {$data['do']} AND" : false;
    $w = ($cat || $type || $city || $ot || $do) ? 'WHERE ' : false;
    if ($w) {
        $where = "SELECT * FROM `items` $w$cat$type$city$ot$do";
        $where = substr($where, 0, -3);
    }else{
        $where = "SELECT * FROM `items`";
    }
    return getTable($where);
}

function photoItem($item, $class = '') {
    if (!is_numeric($item)) exit('NO NUMERIC');
    $img = getItem($item)['photo'];
    if ($img == NULL || $img == '') return '<img class="'.$class.'" src="/assets/images/nophoto.png" alt="">';
    return '<img src="'.$img.'" alt="" class="'.$class.'">';
}

?>