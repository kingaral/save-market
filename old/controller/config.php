<?php

mb_internal_encoding("UTF-8");
error_reporting(E_ALL);
ini_set("display_errors", 1);
$message = false;
$user = (isset($_SESSION['user'])) ? $_SESSION['user'] : false;
date_default_timezone_set('Asia/Almaty');
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "root");
define("DB_NAME", "finder");
define("FORMAT_DATE", "Y.m.d H:i:s");
?>