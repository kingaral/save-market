ymaps.ready(init);
var myMap;

function init () {
    myMap = new ymaps.Map("map", {
        center: [43.236251, 76.897844], // Алматы
        zoom: 12
    }, {
        balloonMaxWidth: 200,
        searchControlProvider: 'yandex#search'
    });

    myMap.events.add('click', function (e) {
        if (!myMap.balloon.isOpen()) {
            var coords = e.get('coords');
            var coor = [coords[0].toPrecision(6), coords[1].toPrecision(6)].join(', ');
            myMap.balloon.open(coords, {
                contentHeader:'Расположение',
                contentBody: '<p>Координата: ' + coor + '</p>'
            });
            $('#coors').val(coor);
        }
        else {
            myMap.balloon.close();
        }
    });

    myMap.events.add('contextmenu', function (e) {
        myMap.hint.open(e.get('coords'), 'Кто-то щелкнул правой кнопкой');
    });

    // Скрываем хинт при открытии балуна.
    myMap.events.add('balloonopen', function (e) {
        myMap.hint.close();
    });
}



